# README #

Spring version of Silo Server. Primarily created for Raspberry Pi.

## Dependencies ##
* mysql: user - root, pass - root, database - agriculture
* java 8

## Raspberry Pi preparation steps ##
* sudo apt-get update
* sudo apt-get install mysql-server && sudo apt-get install oracle-java8-jdk
* cd /tmp/ && wget http://www.mirrorservice.org/sites/ftp.apache.org/maven/maven-3/3.2.5/binaries/apache-maven-3.2.5-bin.tar.gz &&sudo tar -xzvf /tmp/apache-maven-3.2.5-bin.tar.gz &&  sudo mv apache-maven-3.2.5 /opt/
* sudoedit /etc/profile.d/maven.sh
* export M2_HOME=/opt/apache-maven-3.2.5
* export PATH=$PATH:$M2_HOME/bin
* sudo apt-get install git && mkdir -p ~/git && cd ~/git &&  git clone https://zd3no@bitbucket.org/zd3no/spring-boot-silo-monitor.git && cd spring-boot-silo-monitor && mvn clean install && cp target/spring-boot-silo-monitor*.war ~/ && cd ~/

* To start application run : java -jar spring-boot-silo-monitor-1.0-SNAPSHOT.war
* OPTIONAL: curl -sSL https://get.docker.com | sh

## mysql password issues after install ##
* sudo mysql_secure_installation
* sudo mysql -u root -p // set your password and exit
* sudo mysql -u root -p
* create databse agriculture; use agriculture; GRANT ALL PRIVILEGES ON agriculture.* TO 'pi'@'localhost' IDENTIFIED BY 'root'; GRANT ALL PRIVILEGES ON agriculture.* TO 'root'@'localhost' IDENTIFIED BY 'root';