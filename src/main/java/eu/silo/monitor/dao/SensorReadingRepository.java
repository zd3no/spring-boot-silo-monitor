
package eu.silo.monitor.dao;


import eu.silo.monitor.entities.SensorReading;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SensorReadingRepository extends JpaRepository<SensorReading, Integer>, SensorReadingRepositoryCustom {

}
