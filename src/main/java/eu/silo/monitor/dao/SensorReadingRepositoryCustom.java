package eu.silo.monitor.dao;


import java.util.Collection;
import java.util.Date;
import java.util.List;

interface SensorReadingRepositoryCustom {

    /**
     * Return all entities from DB that match the provided class sorted by the provided sort string in the provided sort order.
     *
     * @param clazz  - The entity class to extract from DB
     * @param sortBy - Sort string attribute of the provided class
     * @param sort   - Sort type enum (ASC or DESC)
     * @return - Collection of entities, or empty collection
     * @throws ClassNotFoundException - If the specified class is not a valid Entity
     */
    Collection<?> getAllSorted(Class<?> clazz, String sortBy, Sort sort) throws ClassNotFoundException;

    /**
     * Get all entities between dates sorted by type in given order.
     *
     * @param clazz  - the class of the entity
     * @param from   - from Date
     * @param to     - to Date
     * @param sortBy - Sort string attribute of the provided class
     * @param sort   - Sort type enum (ASC or DESC)
     * @return - Collection of entities, or empty collection
     */
    List<?> getReadingsBetweenDatesSorted(Class<?> clazz, Date from, Date to, String sortBy, Sort
            sort);
}
