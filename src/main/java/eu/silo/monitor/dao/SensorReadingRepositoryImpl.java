package eu.silo.monitor.dao;


import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Repository
@Transactional(readOnly = true)
class SensorReadingRepositoryImpl implements SensorReadingRepositoryCustom {

    @PersistenceContext
    EntityManager em;

    @Override
    public Collection<?> getAllSorted(Class<?> clazz, String sortBy, Sort sort) throws ClassNotFoundException {
        return em.createQuery("SELECT x FROM " + clazz.getCanonicalName() + " as x ORDER BY x." + sortBy + " " + sort.name(), clazz).getResultList();
    }

    @Override
    public List<?> getReadingsBetweenDatesSorted(final Class<?> clazz, final Date from, final Date to, final String sortBy, Sort
            sort) {
        Date endDate = to;
        Date startDate = from;
        if (from.after(to)) {
            endDate = to;
            startDate = from;
        } else if (from.before(to)) {
            endDate = from;
            startDate = to;
        }
        return em.createQuery("SELECT x FROM " + clazz.getCanonicalName() + " as x WHERE x.date between :endDate AND :startDate ORDER BY x." +
                sortBy + " " + sort.name(), clazz)
                .setParameter("startDate", startDate)
                .setParameter("endDate", endDate)
                .getResultList();
    }
}
