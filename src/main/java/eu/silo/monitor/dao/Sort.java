package eu.silo.monitor.dao;

public enum Sort {
    ASC, DESC
}