package eu.silo.monitor.rest;

import eu.silo.monitor.dao.SensorReadingRepository;
import eu.silo.monitor.dao.Sort;
import eu.silo.monitor.entities.SensorReading;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/rest/reading")
class SensorReadingController {

    private final SensorReadingRepository repository;

    private static final Logger log = LoggerFactory.getLogger(SensorReadingController.class);

    @Autowired
    public SensorReadingController(final SensorReadingRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(consumes = "application/json", produces = "text/plain", method = RequestMethod.PUT)
    public ResponseEntity<Object> persistSensorReadings(
            @RequestBody List<SensorReading> sensorReadings) {
        if (sensorReadings == null || sensorReadings.isEmpty()) {
            log.error("Tried to persist null or empty Collection<SensorReading>");
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        log.info("Recording {} sensor readings to database.", sensorReadings.size());
        List<SensorReading> successfulPersist = repository.save(sensorReadings);
        HttpStatus status;
        if (successfulPersist != null && !successfulPersist.isEmpty()) {
            status = HttpStatus.CREATED;
        } else {
            log.warn("Unsuccessful write to database");
            status = HttpStatus.NOT_MODIFIED;
        }
        return ResponseEntity.status(status).build();
    }

    @RequestMapping(path = "/{id}", consumes = "application/json", produces = "text/plain",
            method = RequestMethod.PUT)
    public ResponseEntity<Object> persistSensorReading(@PathVariable("id") String id,
                                                       @RequestBody SensorReading sensorReading) {
        if (sensorReading == null) {
            log.error("Tried to persist null SensorReading with id {}", id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }

        log.info("Recording {} sensor reading to database.", id);
        SensorReading successfulPersist = repository.save(sensorReading);
        HttpStatus status;
        if (successfulPersist != null) {
            status = HttpStatus.CREATED;
        } else {
            log.warn("Sensor reading {} was not writen to database", id);
            status = HttpStatus.NOT_MODIFIED;
        }
        return ResponseEntity.status(status).build();
    }

    @RequestMapping(path = "/", produces = "application/json", method = RequestMethod.GET)
    public ResponseEntity<List<SensorReading>> getAll() {
        try {
            HttpStatus status;
            @SuppressWarnings("unchecked")
            List<SensorReading> list = (List<SensorReading>) repository.getAllSorted(SensorReading.class, "date", Sort.DESC);
            if (list.isEmpty()) {
                status = HttpStatus.NO_CONTENT;
            } else {
                status = HttpStatus.OK;
            }
            return ResponseEntity.status(status).body(list);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @RequestMapping(path = "/between", produces = "application/json", method = RequestMethod.GET)
    public ResponseEntity<List<SensorReading>> getBetweenDates(@RequestParam("from") Date fromDate, @RequestParam("to") Date toDate) {
        HttpStatus status;
        List<SensorReading> result =
                (List<SensorReading>) repository.getReadingsBetweenDatesSorted(SensorReading.class, fromDate, toDate, "date", Sort.DESC);
        if (result.isEmpty()) {
            status = HttpStatus.NO_CONTENT;
        } else {
            status = HttpStatus.OK;
        }
        return ResponseEntity.status(status).body(result);
    }
}
