define(function () {

    return function ReadingCollection() {
        this.keys = [];
        this.data = [];

        this.add = function (reading) {
            if (reading.active) {
                var readingDate = new Date(reading.date);
                var readingDateMonthDay = readingDate.getDate() < 10 ? "0" + readingDate.getDate() : readingDate.getDate();
                var readingDateMonth = readingDate.getMonth() < 10 ? "0" + readingDate.getMonth() : readingDate.getMonth();
                this.readingKey = "_" + readingDateMonthDay + "_" + readingDateMonth;
                if (this.keys.indexOf(this.readingKey) >= 0) {
                    this[this.readingKey].count += 1;
                    this[this.readingKey].temperature += reading.temperature;
                    this[this.readingKey].temperatureArray.push(reading.temperature);
                    this[this.readingKey].humidity += reading.humidity;
                } else {
                    this.keys.push(this.readingKey);
                    this[this.readingKey] = new Object();
                    this[this.readingKey].count = 1;
                    this[this.readingKey].temperature = reading.temperature;
                    this[this.readingKey].humidity = reading.humidity;
                    this[this.readingKey].temperatureArray = [];
                    this[this.readingKey].temperatureArray.push(reading.temperature);
                }
                this.data.push(reading);
            }
        };

        this.getAveragedReadingsCollection = function () {
            var collection = [];
            for (var i = 0; i < this.keys.length; i++) {
                var temp = this[this.keys[i]].temperature / this[this.keys[i]].count;
                var hum = this[this.keys[i]].humidity / this[this.keys[i]].count;
                var object = {
                    temperature: temp,
                    humidity: hum
                };
                collection.push(object);
            }
            return collection;
        };

        this.getLast24hData = function () {
            var biggestDate = new Date(Math.max.apply(Math, this.data.map(function (o) {
                return o.date;
            })));
            var twentyFourHoursFromBiggestDate = biggestDate - 24 * 60 * 60 * 1000;
            return this.data.filter(function (reading) {
                if (reading.date >= twentyFourHoursFromBiggestDate) {
                    return reading;
                }
            });
        };

        this.getMaxTemperatureData = function () {
            var collection = [];
            for (var i = 0; i < this.keys.length; i++) {
                var temp = this[this.keys[i]].temperatureArray;
                var maxTemperature = null;
                for (var j = 0; j < temp.length; j++) {
                    if (maxTemperature === null || maxTemperature < temp[j])
                        maxTemperature = temp[j];
                }
                collection.push(maxTemperature);
            }
            return collection;
        };

        this.getMinTemperatureData = function () {
            var collection = [];
            for (var i = 0; i < this.keys.length; i++) {
                var temp = this[this.keys[i]].temperatureArray;
                var minTemperature = null;
                for (var j = 0; j < temp.length; j++) {
                    if (minTemperature === null || minTemperature > temp[j])
                        minTemperature = temp[j];
                }
                collection.push(minTemperature);
            }
            return collection;
        };
    }
});